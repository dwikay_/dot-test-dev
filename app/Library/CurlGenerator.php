<?php

namespace App\Library;

class CurlGenerator {

  public function getIndex($urls){

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");

    $content_type = "application/json";

    // dd($url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'key: 0df6d5bf733214af6c6644eb8717c92c'
      )
    );

      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);
      $asd = json_decode($output);

      return $asd;

  }

}

?>
