<?php

namespace App\Http\Controllers\City;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Library\CurlGenerator;

class CityController extends Controller
{
  public function getCity(CurlGenerator $curlGen, Request $request){

    $url = "/starter/city";
    $param = $curlGen->getIndex($url);
    $arrObj = $param->rajaongkir->results;
    $searchedValue = $request['searchKey'];

    if($request['searchKey'] == null){
      $vals = $arrObj;
      return $vals;
    }else{
      $neededObject = array_filter(
      $arrObj,
        function ($e) use ($searchedValue) {
          if($e->city_id == $searchedValue){
            $arrVals = array(
              "city_id" => $e->city_id,
              "province_id" => $e->province_id,
              "province" => $e->province,
              "type" => $e->type,
              "city_name" => $e->city_name,
              "postal_code" => $e->postal_code
            );
            return $arrVals;
          }
        }
      );
      return json_encode($neededObject[$searchedValue-1]);
    }
  }
}
