<?php

namespace App\Http\Controllers\Province;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Library\CurlGenerator;

class ProvinceController extends Controller
{
  public function getProvince(CurlGenerator $curlGen, Request $request){

    $url = "/starter/province";
    $param = $curlGen->getIndex($url);
    $arrObj = $param->rajaongkir->results;
    $searchedValue = $request['searchKey'];

    if($request['searchKey'] == null){
      $vals = $arrObj;
      return $vals;
    }else{
      $neededObject = array_filter(
      $arrObj,
        function ($e) use ($searchedValue) {
          if($e->province_id == $searchedValue){
            $arrVals = array(
              "province_id" => $e->province_id,
              "province" => $e->province
            );
            return $arrVals;
          }
        }
      );
      return json_encode($neededObject[$searchedValue-1]);
    }
  }
}
